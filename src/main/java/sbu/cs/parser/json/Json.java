package sbu.cs.parser.json;

import java.util.List;

public class Json implements JsonInterface {

    List<ELEMENTS> elements;

    public Json(List<ELEMENTS> elements)
    {
        this.elements = elements;
    }


    @Override
    public String getStringValue(String key) {
        // TODO implement this

        for(int i = 0; i < elements.size(); i++)
        {
            if(elements.get(i).getKey().equals(key))
            {
                if(elements.get(i).getValue().indexOf('[') >= 0)
                {
                    return elements.get(i).getValue().replaceAll(" ", ", ");
                }
                else
                {
                    return elements.get(i).getValue();
                }
            }
        }

        return null;
    }
}