package sbu.cs.parser.json;

public class JsonDoubleFunction extends ELEMENTS
{
    private double value;

    @Override

    public String getValue()
    {
        return Double.toString(value);
    }

    public void setValue(double value)
    {
        this.value = value;
    }


}
