package sbu.cs.parser.json;

public class JsonBooleanFunction extends ELEMENTS
{
    private boolean value;

    @Override
    public String getValue()
    {
        return Boolean.toString(value);
    }

    public void setValue(boolean value)
    {
        this.value = value;
    }

}
