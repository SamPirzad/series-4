package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.List;


public class JsonParser {

    /*
    * this function will get a String and returns a Json object
     */
    public static Json parse(String data) {
        // TODO implement this
        List<ELEMENTS> elements = new ArrayList<>();

        data = data.replaceAll("[\\s\"{}]", "");

        if(data.indexOf('[') >= 0)
        {
            data = CharReplacer(data);
        }

        String[] segment = data.split(",");

        for(int i = 0; i < segment.length; i++)
        {
            String[] template = segment[i].split(":");

            if(template[1].matches("\\d*"))
            {
                integerDeatails(elements, template);
            }

            else if(template[1].matches("\\d*\\.\\d*"))
            {
                doubleDeatails(elements, template);
            }

            else if(template[1].matches("(true|false)"))
            {
                booleanDeatails(elements, template);
            }

            else
            {
                stringDeatails(elements, template);
            }
        }

        return new Json(elements);

        //return null;
    }

    private static String CharReplacer(String data)
    {
        String temp = data;

        while(temp.indexOf('[') >= 0)
        {
            String str = temp.substring(temp.indexOf("["), temp.indexOf("]")+1);
            data = data.replace(str, str.replaceAll(",", " "));
            temp = temp.replace(str, "");
        }

        return data;
    }

    private static List<ELEMENTS> integerDeatails(List<ELEMENTS> elements, String[] tmp)
    {
        JsonIntegerFunction Value = new JsonIntegerFunction();

        Value.setKey(tmp[0]);
        Value.setValue(Integer.parseInt(tmp[1]));

        elements.add(Value);

        return elements;
    }

    private static List<ELEMENTS> doubleDeatails(List<ELEMENTS> elements, String[] tmp)
    {
        JsonDoubleFunction Value = new JsonDoubleFunction();

        Value.setKey(tmp[0]);
        Value.setValue(Double.parseDouble(tmp[1]));

        elements.add(Value);

        return elements;
    }

    private static List<ELEMENTS> stringDeatails(List<ELEMENTS> elements, String[] tmp)
    {
        JsonStringFunction Value = new JsonStringFunction();

        Value.setKey(tmp[0]);
        Value.setValue(tmp[1]);

        elements.add(Value);

        return elements;
    }

    private static List<ELEMENTS> booleanDeatails(List<ELEMENTS> elements, String[] tmp)
    {
        JsonBooleanFunction Value = new JsonBooleanFunction();

        Value.setKey(tmp[0]);
        Value.setValue(Boolean.parseBoolean(tmp[1]));

        elements.add(Value);

        return elements;
    }





    /*
    * this function is the opposite of above function. implementing this has no score and
    * is only for those who want to practice more.
     */
    public static String toJsonString(Json data) {
        return null;
    }
}
