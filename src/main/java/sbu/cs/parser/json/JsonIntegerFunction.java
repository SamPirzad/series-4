package sbu.cs.parser.json;

public class JsonIntegerFunction extends ELEMENTS
{
    private long value;

    @Override

    public String getValue()
    {
        return Long.toString(value);
    }

    public void setValue(int value)
    {
        this.value = value;
    }

}
