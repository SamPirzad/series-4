package sbu.cs.parser.html;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;


public class Node implements NodeInterface {

     List<Node> children;
     Map<String, String> attributes;
     String input;
     String document;
     boolean SelfDistructive;
     String tagName;

    public Node(String document)
    {
        this.document = document;

        tagName = TagNameExtractor();
        SelfDistructive = SelfDistructiveExtractor();
        input = extractInside();
        attributes = setAttributes();
        children = ChildAdder();
    }

    private int GetStartPoint(int point)
    {
        int i = point;
        while(i < input.length() && input.charAt(i) != '<') {
            i++;
        }
        return i;
    }

    private int GetEndPoint(int point)
    {
        int j = point;
        while(j < input.length() && input.charAt(j) != '>') {
            j++;
        }
        return j;
    }

    private String GetTagName(int Start, int End)
    {
        int j = Start+1;
        while(j <= End)
        {
            if(input.charAt(j) != ' ' && input.charAt(j) != '>') {
                j++;
            }
            else {
                break;
            }
        }
        return input.substring(Start+1, j);
    }

    private List<Node> ChildAdder()
    {
        List<Node> child = new ArrayList<>();
        int i = 0;
        int inputSize = input.length();
        int TagStart;
        int TagEnd;
        boolean SelfDistructiveTag;

        while(i < inputSize)
        {
            TagStart = GetStartPoint(i);
            if(TagStart == inputSize)
                return new ArrayList<Node>();

            TagEnd = GetEndPoint(TagStart);
            SelfDistructiveTag = (input.charAt(TagStart+1) == '/');

            if(SelfDistructiveTag)
            {
                child.add(new Node(input.substring(TagStart, TagEnd+1)));
                i = TagEnd + 1;
            }
            else
            {
                String TagName = GetTagName(TagStart, TagEnd);
                int count = 1;
                i = TagEnd+1;

                while(count != 0)
                {
                    int TagStartPrime = GetStartPoint(i);
                    int tempTagEndPrime = GetEndPoint(TagStartPrime);
                    String TagNamePrime = GetTagName(TagStartPrime, tempTagEndPrime);

                    if(TagName.equals(TagNamePrime))
                        count++;
                    else if(("/" + TagName).equals(TagNamePrime))
                        count--;

                    i = tempTagEndPrime + 1;
                }

                child.add(new Node (input.substring(TagStart, i)));
            }
        }
        return child;
    }

    private Map<String, String> setAttributes()
    {
        Map<String, String> newAttributes = new HashMap<>();
        int i = 0;
        String insideTag = getTagInside();

        if(insideTag.indexOf('=') < 0)
        {
            return newAttributes;
        }

        int KeyStart;
        int KeyEnd;
        int startOfValue;
        int endOfValue;

        while(i < insideTag.length())
        {
            KeyStart = i;

            while(i < insideTag.length() && insideTag.charAt(i) != '=')
                i++;

            KeyEnd = i;
            startOfValue = KeyEnd + 2;

            i = startOfValue;
            while(i < insideTag.length() && insideTag.charAt(i) != '\"')
                i++;

            endOfValue = i;

            newAttributes.put(insideTag.substring(KeyStart, KeyEnd),
                    insideTag.substring(startOfValue, endOfValue));
            i += 2;
        }

        if(i == insideTag.length())
        {
            return null;
        }

        return newAttributes;
    }

    private String getTagInside()
    {
        int i = 0;
        int startingPoint = 0;
        int endingPoint = 0;

        while(i < document.length())
        {
            while(i < document.length() && document.charAt(i) != '<')
                i++;

            while(i < document.length() && document.charAt(i) != ' ')
                i++;

            startingPoint = i+1;

            while(i < document.length() && document.charAt(i) != '>')
                i++;

            endingPoint = i;
            break;
        }

        if(startingPoint == endingPoint+1)
        {
            return "";
        }
        return document.substring(startingPoint, endingPoint);
    }

    private String extractInside()
    {
        if(!SelfDistructive)
        {
            int start = 0;
            int end = 0;
            int i = 0;
            int DocSize = document.length();

            while(i < DocSize)
            {
                while(i < DocSize && document.charAt(i) != '>')
                    i++;

                start = i+1;
                i = DocSize-2;
                while(i > 0 && document.charAt(i) != '<')
                    i--;

                end = i;
                break;
            }
            return document.substring(start, end);
        }
        return "";
    }

    private boolean SelfDistructiveExtractor()
    {
        for(int i = 0; i < document.indexOf(">")-1; i++)
        {
            if(document.charAt(i) == '<')
            {
                if(document.charAt(i+1) == '/')
                    return true;

                else
                    return false;

            }
        }
        return false;
    }

    private String TagNameExtractor()
    {
        int start = 0;
        int end = 0;

        for(int i = 0; i < document.length()-1; i++)
        {
            if(document.charAt(i) == '<' )
            {
                start = i+1;
                break;
            }
        }
        for(int i = start; i < document.length(); i++)
        {
            if(document.charAt(i) == ' ' || document.charAt(i) == '>')
            {
                end = i;
                break;
            }
        }
        tagName = document.substring(start, end);

        if(tagName.contains("/"))
            tagName = tagName.replace("/", "");


        return tagName;
    }


    /*
    * this function will return all that exists inside a tag
    * for example for <html><body><p>hi</p></body></html>, if we are on
    * html tag this function will return <body><p1>hi</p1></body> and if we are on
    * body tag this function will return <p1>hi</p1> and if we are on
    * p tag this function will return hi
    * if there is nothing inside tag then null will be returned
     */
    @Override
    public String getStringInside() {
        // TODO implement this
        if(input.equals(""))
        {
            return null;
        }

        return input;
    }

    /*
    *
     */
    @Override
    public List<Node> getChildren() {

        List<Node> childrenCopy = new ArrayList<>(children);

        return childrenCopy;
    }

    /*
    * in html tags all attributes are in key value shape. this function will get a attribute key
    * and return it's value as String.
    * for example <img src="img.png" width="400" height="500">
     */
    @Override
    public String getAttributeValue(String key) {
        // TODO implement this

        return attributes.get(key);

    }
}
